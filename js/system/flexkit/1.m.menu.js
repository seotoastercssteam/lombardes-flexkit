var body = document.body;

document.body.addEventListener('touchstart', function () {
    var touchPoint = getScrollPosition();

    window.onscroll = function () {
        if (touchPoint > getScrollPosition()) {
            document.querySelector('.mobile-bar').classList.remove('bar-scroll-hide');
        } else if (touchPoint < getScrollPosition()) {
            document.querySelector('.mobile-bar').classList.add('bar-scroll-hide');
        }
    };
});

helper._addEventListener('click', '[data-refer]', function (event, element) {
    tapAction(element);
});

// TODO reorganize this method
tapButton('sub-menu-btn', function (e) {
    if (!window.Hammer) {
        e.stopPropagation();
    } else {
        e.srcEvent.stopPropagation();
    }
    e.preventDefault();
    e.target.classList.toggle('active');
    e.target.parentNode.querySelector('ul').classList.toggle('open');
});

/**
 * Open menu (main or dropdown)
 * @param element
 * @private
 */
function tapAction(element) {
    if (typeof element != 'undefined' && element.dataset.refer.length) {
        var referName = element.dataset.refer;
        document.querySelector('[data-el="' + referName + '"]').classList.add('open');
        body.setAttribute('data-overlay', '');
        body.addEventListener('touchend', _closeMenu);
    }
}

/**
 * Close menu if click outside of menu
 * @param event
 * @private
 */
function _closeMenu(event) {
    if (event.target.hasAttribute('data-overlay')) {
        event.preventDefault();

        document.querySelector('.m-menu.open').classList.remove('open');
        body.removeAttribute('data-overlay');
    }
}

function tapButton(selector, fun) {
    var listItems = document.getElementsByClassName(selector);
    if (!window.Hammer) {
        for (var i = 0; i < listItems.length; i++) {
            if (window.addEventListener) {
                listItems[i].addEventListener('click', fun, false);
            } else if (window.attachEvent) {
                listItems[i].attachEvent('click', fun, false);
            }
        }
    } else {
        Hammer.each(listItems, function (item) {
            var touchControl = new Hammer(item, {domEvents: true});
            touchControl.on("tap", fun);
        });
    }
}

/**
 *
 * @returns {Number|number}
 */
function getScrollPosition() {
    return window.pageYOffset || document.documentElement.scrollTop;
}