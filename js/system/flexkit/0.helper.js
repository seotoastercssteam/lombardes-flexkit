/**
 *
 * @constructor
 */
function Helper(){

}

/**
 *
 * @param event
 * @param selectorList
 * @param callback
 * @param parentElement
 * @private
 */
Helper.prototype._addEventListener = function(event, selectorList, callback, parentElement){ // TODO do more tests

    parentElement = document.querySelector(parentElement) || document;

    var _selectorList = selectorList.split(','),
        _elementsMap = [],
        elements,
        i = 0,
        m = 0;

    for (; i < _selectorList.length; i++) {
        elements = parentElement.querySelectorAll(_selectorList[i].trim());
        for (var n = 0; n < elements.length; n++) {
            _elementsMap.push(elements[n]);
        }
    }
    for(; m < _elementsMap.length; m++){
        _elementsMap[m].addEventListener(event, function (event) {
            callback(event, event.target);
        }, false);
    }
};

/**
 *
 * @type {Helper}
 */
var helper  = new Helper();