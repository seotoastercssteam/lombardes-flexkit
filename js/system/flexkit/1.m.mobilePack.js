// Call functions for touch devices
if (device.type) {
    replaceSelector(':hover|:active', '.touch', false);    // Replace :hover => .touch for touch devices
    // TODO optimize events
    document.body.addEventListener('touchstart', function (e) {
        e.target.classList.add('touch');
    });
    document.body.addEventListener('touchend', function (e) {
        e.target.classList.remove('touch');
    });
    document.body.addEventListener('touchmove', function (e) {
        e.target.classList.remove('touch');
    });
}

if (device.type) {
    // TODO optimize structure of all file
    var contactClick = false;
    helper._addEventListener('click', '[href^="mailto:"], [href^="tel:"]', function () {
        contactClick = true;
    });

    window.onbeforeunload = function () {
        if (!contactClick)
            showLoading(device.type);
        contactClick = false;
    }
    window.onload = hideLoading(device.type);
}

function showLoading() {
    document.documentElement.classList.remove('loaded');
}

function hideLoading() {
    document.documentElement.classList.add('loaded');
}

if (device.mobile() || device.tablet()) {
    WebFontConfig = {
        google: {
            families: ['Roboto:400,500']
        }
    };
    (function (d) {
        var wf = d.createElement('script'), s = d.scripts[0];
        wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js';
        s.parentNode.insertBefore(wf, s);
    })(document);
}